// Connection URL
var url = 'mongodb://localhost:27017/devops';

var MongoClient = require('mongodb').MongoClient, assert = require('assert');

describe("Module Math", () => {
  describe("GET /math/add", function() {
    it('should add two values', function() {
      return server.run('/math/add/3/4').then((res) => {
        res.statusCode.should.equal(200);
        res.result.should.equal('7');
      })
    })

    it('should be rejected on wrong term1', function() {
      return server.run('/math/add/foo/2').then(expectBadRequest)
    })

    it('should be rejected on wrong term2', function() {
      return server.run('/math/add/3/foo').then(expectBadRequest)
    })

    it('should add decimals and numbers', function() {
      return server.run('/math/add/-1/1.15').then((res) => {
        res.statusCode.should.equal(200);
        res.result.should.equal('0.15');
      })
    })

    it('should be saved in DB', function(){
      testDB('add',1,1, function(res){
        res.should.deep.equal([{term1: 1, term2: 1, oper: '+'}]);
      })
    })
  })

  describe("GET /math/multiply", function() {
    it('should multiply two values', function() {
        return server.run('/math/multiply/3/4').then((res) => {
                res.statusCode.should.equal(200);
        res.result.should.equal(12);
    })
    })

    it('should be rejected on wrong term1', function() {
        return server.run('/math/multiply/foo/2').then(expectBadRequest)
    })

    it('should be rejected on wrong term2', function() {
        return server.run('/math/multiply/3/foo').then(expectBadRequest)
    })

    it('should be saved in DB', function(){
      testDB('multiply',2,2, function(res){
        res.should.deep.equal([{term1: 2, term2: 2, oper: 'x'}]);
      })
    })
  })

  describe("GET /math/divide", function() {
    it('should divide two int values', function() {
      return server.run('/math/divide/8/4').then((res) => {
            res.statusCode.should.equal(200);
      res.result.should.equal(2);
    })
    })

    it('should divide two float values', function() {
      return server.run('/math/divide/8.4/2.1').then((res) => {
            res.statusCode.should.equal(200);
      res.result.should.equal(4);
    })
    })

    it('should be rejected on wrong dividend', function() {
      return server.run('/math/divide/foo/2').then(expectBadRequest)
    })

    it('should be rejected on wrong divisor', function() {
      return server.run('/math/divide/3/foo').then(expectBadRequest)
    })

    it('should be rejected when ', function() {
      return server.run('/math/divide/3/foo').then(expectBadRequest)
    })

    it('should be saved in DB', function(){
      testDB('divide',3,3, function(res){
        res.should.deep.equal([{term1: 3, term2: 3, oper: '/'}]);
      })
    })
  })

  describe("GET /math/sub", function() {
    it('should sub two int values', function() {
      return server.run('/math/sub/8/4').then((res) => {
            res.statusCode.should.equal(200);
      res.result.should.equal('4');
    })
    })

    it('should sub two float values', function() {
      return server.run('/math/sub/8.4/2.1').then((res) => {
            res.statusCode.should.equal(200);
      res.result.should.equal('6.3');
    })
    })

    it('should be rejected on wrong suber', function() {
      return server.run('/math/sub/foo/2').then(expectBadRequest)
    })

    it('should be rejected on wrong subeur', function() {
      return server.run('/math/sub/3/foo').then(expectBadRequest)
    })

    it('should be saved in DB', function(){
      testDB('sub',4,4, function(res){
        res.should.deep.equal([{term1: 4, term2: 4, oper: '-'}]);
      })
    })
  })

  // ---------------------------------------------------------------------------
  // Some methods that will help us with the tests
  // ---------------------------------------------------------------------------
  function expectBadRequest(res) {
    res.statusCode.should.equal(400);
    res.result.error.should.equal("Bad Request");
    expect(res.result.error).to.be.equal("Bad Request");
  }

  function testDB(stringOper, term1, term2, callback){
    var result = []
    server.run('/math/'+stringOper+'/'+term1+'/'+term2);
    MongoClient.connect(url, function(err, db) {
      // Get the documents collection
      var collection = db.collection('mathHistory');

      data = collection.find({},
          {term1:1, term2:1, oper:1, _id:0}).sort(
          { _id: -1 } ).limit(1).toArray(function(err, doc){ callback(doc) });
    });
  }
})