describe("Module Rectangle", () => {
    describe("GET /rectangle", function () {
        it('should reject when `width` parameter is not a number', function () {
            return server.run('/rectangles/a').then((res) => {
                res.statusCode.should.equal(400);
            })
        })

        testRectangles = [
            {
                barSequences: "2,3,1,4",
                area: "4",
                result: [
                    '4       $',
                    '3   $   $',
                    '2 $ $   $',
                    '1 $ $ $ $',
                    '+ 1 2 3 4'
                ].join("\n")
            },
            {
                barSequences: "2,3,1,4,8",
                area: "8",
                result: [
                    '8         $',
                    '7         $',
                    '6         $',
                    '5         $',
                    '4       $ $',
                    '3   $   $ $',
                    '2 $ $   $ $',
                    '1 $ $ $ $ $',
                    '+ 1 2 3 4 5'
                ].join("\n")
            }, {
                barSequences: "1",
                area: "1",
                result: [
                    '1 $',
                    '+ 1'
                ].join("\n")
            }
        ]

        testRectangles.forEach(function (rectangle) {
            it(`should show correct result on sequence = ${rectangle.barSequences}`, function () {
                return server.run({
                    method: 'GET',
                    url: '/rectangles/'+rectangle.barSequences
                }).then(function (res) {
                    res.statusCode.should.equal(200)
                    res.result.should.contains(rectangle.result)
                    res.result.should.contains("Aire: "+rectangle.area)
                })
            })
        })
    })
})