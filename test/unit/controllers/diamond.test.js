describe("Module Diamond", () => {
    describe("GET /diamond", function () {
        it('should redirect to /diamond/new', function () {
            return server.run('/diamond').then((res) => {
                res.statusCode.should.equal(302);
                res.headers.location.should.equal('/diamond/new');
            })
        })

        describe("GET /diamond/new", function () {
            it('should show a form with an input and a submit', function () {
                return server.run('/diamond/new').then((res) => {
                    res.statusCode.should.equal(200);
                    res.result.should.contains('<input')
                    res.result.should.contains('<input type="submit"')
                })
            })
        })

        describe("GET /diamond", function () {
            it('should reject when `width` parameter is not a number', function () {
                return server.run({
                    method: 'GET',
                    url: '/diamond/a'
                }).then(expectBadRequest)
            })

            // odd = impair
            it('should reject when `width` parameter is not odd', function () {
                return server.run({
                    method: 'GET',
                    url: '/diamond/2'
                }).then(expectBadRequest)
            })

            testDiamonds = [{
                width: 1,
                result: '+'
            }, {
                width: 3,
                result: [
                    '.+.',
                    '+++',
                    '.+.'
                ].join("\n")
            }, {
                width: 5,
                result: [
                    '..+..',
                    '.+++.',
                    '+++++',
                    '.+++.',
                    '..+..'
                ].join("\n")
            }]

            testReverseDiamonds = [{
                width: -1,
                result: '.'
            }, {
                width: -3,
                result: [
                    '+.+',
                    '...',
                    '+.+'
                ].join("\n")
            }, {
                width: -5,
                result: [
                    '++.++',
                    '+...+',
                    '.....',
                    '+...+',
                    '++.++'
                ].join("\n")
            }]
            // C'est possible de créér des tests unitaires de façon dynamique
            testDiamonds.forEach(function (diamond) {
                it(`should show correct result on width = ${diamond.width}`, function () {
                    return server.run({
                        method: 'GET',
                        url: '/diamond/' + diamond.width
                    }).then(function (res) {
                        res.statusCode.should.equal(200)
                        res.result.should.equal(diamond.result)
                    })
                })
            })

            testReverseDiamonds.forEach(function (diamond) {
                it('should invert + and . when given `width` is negative', function () {
                    return server.run({
                        method: 'GET',
                        url: '/diamond/' + diamond.width
                    }).then(function (res) {
                        res.statusCode.should.equal(200)
                        res.result.should.equal(diamond.result)
                    })
                })
            })
        })

        // ---------------------------------------------------------------------------
        // Some methods that will help us with the tests
        // ---------------------------------------------------------------------------
        function expectBadRequest(res) {
            res.statusCode.should.equal(400);
            res.result.error.should.equal("Bad Request");
            expect(res.result.error).to.be.equal("Bad Request");
        }
    })
})