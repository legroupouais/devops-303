const path = require('path');

// On défini toutes les routes de notre API
module.exports = [
  {
    method: 'GET',
    path: '/',
    handler: getHandler('index')
  },
  {
    method: 'GET',
    path: '/math/add/{term1}/{term2}',
    handler: getHandler('math', 'getAdd')
  },
  {
    method: 'GET',
    path: '/math/sub/{term1}/{term2}',
    handler: getHandler('math', 'getSub')
  },
  {
    method: 'GET',
    path: '/math/divide/{term1}/{term2}',
    handler: getHandler('math', 'getDivide')
  }, {
    method: 'GET',
    path: '/math/multiply/{term1}/{term2}',
    handler: getHandler('math', 'getMultiply')
  }, {
    method: 'GET',
    path: '/math/history',
    handler: getHandler('math', 'getHistory')
  },
  {
    method: 'GET',
    path: '/diamond',
    handler: getHandler('diamond', 'index')
  },
  {
    method: 'GET',
    path: '/diamond/{width}',
    handler: getHandler('diamond', 'generate')
  },
    {
    method: 'GET',
    path: '/diamond/new',
    handler: getHandler('diamond', 'showForm')
  },
    {
    method: 'POST',
    path: '/diamond/new',
    handler: getHandler('diamond', 'redirect')
  },
    {
    method: 'GET',
    path: '/rectangles/{barSequences}',
    handler: getHandler('rectangle', 'generate')
  },
  {
      method: 'GET',
      path: '/rectangles/{largeur}/{hauteur}',
      handler: getHandler('rectangle', 'generateRandom')
  }

]

// getHandler est la fonction qui nous permet de retourner un handler,
// c'est à dire une méthode qui sera appelé lorsqu'un client ira sur une des
// route. 
function getHandler(controller, action) {
  controller = require(path.join(__dirname, 'controllers', controller));
  return action ? controller[action] : controller;
}