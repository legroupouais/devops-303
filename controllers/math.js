// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
const statsd = require("../config").statd;

const mongoConf = require("../config").mongo;
// Connection URL
var url = 'mongodb://'+mongoConf.host+':'+mongoConf.port+'/'+mongoConf.database;

var MongoClient = require('mongodb').MongoClient, assert = require('assert');

module.exports.getAdd = function (request, reply) {
    statsd.increment("math")
    // parseFloat convert anything to either a float number or NaN
    let term1 = parseFloat(request.params.term1)
    let term2 = parseFloat(request.params.term2)

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    let precision1 = getPrecision(term1)
    let precision2 = getPrecision(term2)

    insertMath(term1, term2, "+")

    reply((term1 + term2).toFixed((precision1 > precision2) ? precision1 : precision2))
}

module.exports.getMultiply = function (request, reply) {
    statsd.increment("math")
    // parseFloat convert anything to either a float number or NaN
    term1 = parseFloat(request.params.term1)
    term2 = parseFloat(request.params.term2)

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    insertMath(term1, term2, "x")

    reply(term1 * term2)
}

module.exports.getDivide = function (request, reply) {
    statsd.increment("math")
    term1 = parseFloat(request.params.term1)
    term2 = parseFloat(request.params.term2)

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    if (term2 === 0) {
        return reply(Boom.badRequest('term2 can not be 0.'))
    }


    insertMath(term1, term2, "/")

    reply(term1 / term2)
}

module.exports.getHistory = function (request, reply) {
    statsd.increment("math")
    let data = ""

    MongoClient.connect(url, function (err, db) {
        assert.equal(null, err);
        console.log("Connected successfully to server")

        // Get the documents collection
        var collection = db.collection('mathHistory');

        collection.find().toArray(function (err, docs) {
            console.log(docs)
            docs.forEach(function (oper) {
                if (oper.term1 != null && oper.term2 != null && oper.oper != null) {
                    data += "<li>" + oper.term1 + " " + oper.oper + " " + oper.term2 + "</li>"
                }
            })
            reply('<!DOCTYPE html><html><head><title></title></head><body><ul>' + data + '</ul></body></html>')
        })
        db.close();
    });
}

function getPrecision(term1) {
    let str = String(term1).split(".")
    if (str.length < 2) {
        return 0
    }
    return str[1].length;
}

module.exports.getSub = function (request, reply) {
    statsd.increment("math")
    let term1 = parseFloat(request.params.term1)
    let term2 = parseFloat(request.params.term2)

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    let precision1 = getPrecision(term1)
    let precision2 = getPrecision(term2)

    insertMath(term1, term2, "-")

    reply((term1 - term2).toFixed((precision1 > precision2) ? precision1 : precision2))
}

var insertMath = function (term1, term2, oper) {
    MongoClient.connect(url, function (err, db) {

        // Get the documents collection
        var collection = db.collection('mathHistory');

        // Insert some documents
        collection.insertOne({term1: term1, term2: term2, oper: oper},
            function (err, result) {
                if (err) console.log("ERR> " + err)
            })
        db.close();
    });
}