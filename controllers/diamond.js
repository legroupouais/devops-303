// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
const statsd = require("../config").statd;

module.exports.index = function (request, reply) {
    statsd.increment("diamond")
    reply.redirect("/diamond/new")
}

module.exports.showForm = function (request, reply) {
    statsd.increment("diamond")
    reply('<!DOCTYPE html><html><head><title></title></head><body><form METHOD="POST"><input type="number" name="width"/><input type="submit"></form></body></html>')
}

module.exports.redirect = function(request, reply)
{
    reply.redirect('/diamond/' + request.payload.width)
}
module.exports.generate = function (request, reply) {
    statsd.increment("diamond")
    let point = "."
    let plus = "+"
    let width = parseInt(request.params.width)

    if (isNaN(width)) {
        return reply(Boom.badRequest("The term need to be a number"))
    }
    if (width % 2 == 0) {
        return reply(Boom.badRequest("The term must be odd"))
    }

    let isNegative = width > 0
    width = Math.abs(width)
    let DiamondData = []
    for (let i = 1; i <= width; i++) {
        let DiamondString = ""
        let x = Math.abs(width - ((i * 2) - 1)) / 2
        for (let j = 0; j < width; j++) {
            //DiamondString += (x <= j || j > width - x) ? "." : "+"
            if (j < x || j >= (width-x)) {
                DiamondString+= (isNegative) ? point : plus
            }else {
                DiamondString+= (isNegative) ? plus : point
            }
        }
        DiamondData.push(DiamondString)
    }
    reply(DiamondData.join('\n'))
}