// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
const statsd = require("../config").statd;

function checkNumbers(tab) {
    for (let i in tab) {
        if (isNaN(tab[i])) {
            return false
        }
    }
    return true
}

function getMax(tab) {
    let max = tab[0]
    let i = 0
    while (i < tab.length) {
        if (max < parseInt(tab[i])) {
            max = tab[i]
            i = 0
        } else {
            i++;
        }
    }
    return max
}

function formatMatrix(rect) {
    for (let index in rect) {
        rect[index] = rect[index].join(" ")
    }

    return `<pre>${rect.join("\n")}</pre>`
}
function drawRect(tab, max) {

    let sequence = []
    // Copy
    for (let i in tab) {
        sequence.push(tab[i])
    }

    // The rect will be tab.length * max
    let rect = []

    for (let i = max; i > 0; i--) {
        let row = []
        for (let j = 0; j < sequence.length; j++) {
            if (sequence[j] == i) {
                row.push("$")
                sequence[j]--;
            } else {
                row.push(' ')
            }
        }
        rect.push(row)
    }

    return rect
}

function addAxes(rect) {

    for (let i = 0; i < rect.length; i++) {
        rect[i].unshift(rect.length - i)
    }

    let finalRow = []
    finalRow.push("+")
    for (let j = 1; j < rect[0].length; j++) {
        finalRow.push(j)
    }
    rect.push(finalRow)

    return rect
}

function renderRectangle(tab, reply) {
    statsd.increment("rectangles")
    let peak = getMax(tab)
    let rect = drawRect(tab, peak)
    let html = '<!DOCTYPE><html><html><head><metacharset="utf-8"/><title>Rectangles</title><style>body{font-family:Courrier;}</style></head><body><toreplace><br />Aire: <area></body></html>';
    html = html.replace('<area>', calcArea(rect, tab))
    rect = addAxes(rect)
    html = html.replace('<toreplace>', formatMatrix(rect))
    reply(html)
}

function calcArea_old(rect) {
    let maxArea = 0
    let row = 1
    let col = 1

    if (rect.length == 0) {
        return 0
    }

    // Row test
    let invert = []
    for (let iRow in rect) {
        if (rect[iRow].indexOf(" ") == -1) {
            for (let iCol in rect[0].length) {
                if (invert[iCol] == undefined) {
                    invert.push(iCol)
                }
                invert[iCol].push(rect[iRow][iCol])
            }
            if (maxArea < rect.length) {
                maxArea = rect.length
            }
        }
    }
    // Column test
    for (let index in invert) {
        if (invert[index].indexOf(" ") != -1) {
            if (maxArea < invert.length) {
                maxArea = invert.length
            }
        }
    }


    //// CAUTION INDEX SATRT TO 1
    //while (row <= rect.length && col <= rect[0].length){
    //
    //}

    return maxArea;
}

function getPeaks(tab) {
    return tab.filter(function (item, pos) {
        return tab.indexOf(item) == pos;
    })
}

function calcArea(rect, tab) {
    let arrayArea = []
    let peaks = getPeaks(tab)
    let maxArea = 0

    for (let i = 0; i < peaks.length; i++) {
        arrayArea.push({peakValue: peaks[i], val: 0})
    }

    for (let colIndex in rect[0]) {
        let tmpPeak = tab[colIndex]
        for (let i = 0; i < arrayArea.length; i++) {

            if (arrayArea[i].peakValue > tmpPeak) {
                arrayArea[i].val = 0
            } else if (tmpPeak >= arrayArea[i].peakValue) {
                arrayArea[i].val = (parseInt(arrayArea[i].val) + parseInt(arrayArea[i].peakValue))
            }

            if (maxArea < arrayArea[i].val) {
                maxArea = arrayArea[i].val
            }
        }
    }

    return maxArea
}

module.exports.generate = function (request, reply) {
    let tab = request.params.barSequences.split(",")

    if (!checkNumbers(tab)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }
    renderRectangle(tab, reply)
}

module.exports.generateRandom = function (request, reply) {
    let largeur = request.params.largeur
    let hauteur = request.params.hauteur

    if (isNaN(largeur) || isNaN(hauteur)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    let tab = []
    for (let i = 0; i < largeur; i++) {
        tab.push(Math.floor((Math.random() * hauteur) + 1))
    }
    renderRectangle(tab, reply)
}