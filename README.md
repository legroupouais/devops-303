# README #

**L'état du projet**

	Ce qui est fait :
		- DO-27 (Nicolas)
		- DO-28 (Luigi, Valentin)	
		- DO-51 (Benjamin)
		- DO-52 (Benoit)
		- DO-53 (Nicolas)
		- DO-60 (Benjamin)
		- DO-61 (Benjamin)		
		- DO-79 (Nicolas)
		- DO-150 (Nicolas)		
		- DO-155 (Benoit)
		- DO-161 (Benjamin)	
		- DO-172 (Nicolas)
		
	Ce qu'il reste à faire :
		- DO-29 (Luigi, Valentin)	
		- DO-30 (Luigi, Valentin)

**Tableaux d'amélioration Kaizen :**

| **Difficulté pour la mise en place du déploiement continu** |
| ------------- |: -------------: | 
| **Condition actuelle / Problème**    |        **Condition cible**        |
| La partie réseau doit nous permettre de faire un déploiement continu mais des problèmes techniques empêchent l’équipe en charge d’avancer correctement.| Communication de l’équipe réseau avec l’équipe Dev.|
| **Dans un monde parfait**       |       **Premières étapes...**       |
| Les personnes en charge de la partie réseau résolvent le problème eux même dans un délai convenable qui permet la reprise normale du projet. | L’équipe Dev aide l’équipe réseau pour pouvoir avancer convenablement. |


| **Connaissance et maitrise de Git par tous les membres de l’équipe** |
| ------------- |: -------------: | 
| **Condition actuelle / Problème**    |        **Condition cible**        |
| Tous les membres de l’équipe ne maitrisent pas complètement l’outil Git.| Tous les membres doivent maitriser l’outil Git pour le bon déroulement du projet. |
| **Dans un monde parfait**       |       **Premières étapes...**       |
| Tous les membres maitrise parfaitement l’outil Git. | Formation de l’équipe à l’outil Git. |


| **Mauvaise compréhension de Gitflow et de son workflow** |
| ------------- |: -------------: | 
| **Condition actuelle / Problème**    |        **Condition cible**        |
| Le travail n'est pas toujours "commit" quand un individu arrête de travailler. | Un "commit" régulier a chaque session de travail. |
| **Dans un monde parfait**       |       **Premières étapes...**       |
| A chaque fois que quelqu'un travaille sur le projet il "commit" quant il arrête. | Commiter le plus possible dans sa branche quitte à les fusionner au moment du rebase. |


| **La description des "commit" doivent être explicite et suffisant** |
| ------------- |: -------------: | 
| **Condition actuelle / Problème**    |        **Condition cible**        |
| On retrouve dans la description des "commit" de la non information sur l'avancement réel. | La description doit nous donner un état d'avancement par rapport au "commit" précédent et les problèmes encore existant. |
| **Dans un monde parfait**       |       **Premières étapes...**       |
| La description est complète sur l'état d'avencement et les problèmes qu'il reste. | Prendre au sérieux la description d'un "commit". |


Le choix et fonctionnement de l'approche de Déploiement Continu utilisée :
	Nous avons choisi Ansible pour le déploiement continue, car c'est une solution libre et performante avec la possibilité de le lié à AWS pour déployé des instances.
	
-----------------

**INFORMATION IMPORTANTE : tout les scripts sont lié à l'API amazon ** 

-----------------

Pré-requis : Python, ansible, man et un compte aws non educate pour la connexion à l'api

-----------------

**Les différentes commandes utilisé :**


**La création d'instances + ajout automatique au docker swarm** (le script se trouve /devops-303/ops/create.yml)


```
$ sudo ansible-playbook create.yml
```

**La découverte automatique des hosts EC2 aws**

```
$ sudo /etc/ansible/ec2.py
```

-----------------

**INFORMATION IMPORTANTE : tout les scripts sont lié à l'API amazon ** 

-----------------

Pré-requis : Python, ansible, man et un compte aws non educate pour la connexion à l'api

-----------------

**Les différentes commandes utilisé :**


**La création d'instances + ajout automatique au docker swarm** (le script ce trouve /devops-303/ops/create.yml)


```
$ sudo ansible-playbook create.yml
```

**La découverte automatique des hosts EC2 aws**

```
$ sudo /etc/ansible/ec2.py
```

**Graphana**
Chaque controller indeque le nombre d'appel
![alt tag](https://scontent-cdg2-1.xx.fbcdn.net/v/t35.0-12/17274045_1645711258778988_571642489_o.png?oh=e4e6d4a5a6f62c3d45b6530f9867a1ed&oe=58E6C7CA)

Les contributeurs au projet :
	FLORES Valentin
	LAPLACE Luigi
	LARROQUE Benoit
	LUVISON Nicolas
	WAGNER Benjamin